import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class SelectedTabService {
    public selectedIndex: Observable<any>;
    private selectedIndexSubject: BehaviorSubject<any>;

    constructor() {
        this.selectedIndexSubject = new BehaviorSubject<any>(JSON.parse(sessionStorage.getItem('selectedIndex')));
        this.selectedIndex = this.selectedIndexSubject.asObservable();
    }

    changeTab(val) {
        sessionStorage.setItem('selectedIndex', val);
        this.selectedIndexSubject.next(val);
    }
}