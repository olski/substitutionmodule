import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class TaskService {
    baseUrl = "http://127.0.0.1:8000/api/tasks/";

    constructor(private http: HttpClient) { }

    getAll(){
        return this.http.get(this.baseUrl)
    }

    update(task) {
        const body = { 
            "task": {
                description: task.description,
                title: task.title,
                date: task.date,
                type_task: task.type_task
            }
        };
        return this.http.patch(this.baseUrl +  task.id + '/', body).subscribe(
            (val) => {
                console.log("PATCH call successful value returned in body", 
                            val);
            },
            response => {
                console.log("PATCH call in error", response);
            },
            () => {
                console.log("The PATCH observable is now completed.");
            });
    }

    deleteByID(id){
        return this.http.delete(this.baseUrl + id + '/');
    }
}
