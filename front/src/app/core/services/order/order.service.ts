import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class OrderService {
    baseUrl = "http://127.0.0.1:8000/api/orders/";

    constructor(private http: HttpClient) { }

    create(body) {
        return this.http.post<any>(this.baseUrl, body)
            .pipe(map(order => {
                return order;
            }));
    }

    getAll() {
        return this.http.get(this.baseUrl)
    }

    getByID(id) {
        return this.http.get(this.baseUrl + id + '/')
    }

    update(order) {
        const body = {
            "order": {
                description: order.description,
                order_name: order.order_name,
                date_start: order.date_start,
                date_until: order.date_until,
                taken_on: order.taken_on,
                finished_on: order.finished_on,
                creator: order.creator,
                performer: order.performer,
                tasks: order.tasks,
                order_type: order.order_type,
                done: order.done
            }
        };
        return this.http.patch(this.baseUrl + order.id + '/', body);
    }

    deleteByID(id) {
        return this.http.delete(this.baseUrl + id + '/')
    }

    getNotReserved(id) {
        return this.http.get<any>(this.baseUrl + 'notReserved/' + id + '/')
    }

    getCreatedById(id) {
        return this.http.get(this.baseUrl + 'creator/' + id + '/')
    }

    getReservedById(id) {
        return this.http.get(this.baseUrl + 'reserved/' + id + '/')
    }
}