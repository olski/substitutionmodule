import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    private currentUserSubject: BehaviorSubject<any>;
    public currentUser: Observable<any>;
    baseUrl = "http://127.0.0.1:8000/api/";

    constructor(private http: HttpClient) {
        this.currentUserSubject = new BehaviorSubject<any>(JSON.parse(sessionStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue() {
        return this.currentUserSubject.value;
    }

    login(username, password) {
        var body = { username: username, password: password };


        return this.http.post<any>(this.baseUrl + "auth/", body)
            .pipe(map(user => {
                // store user details and jwt token in local storage to keep user logged in between page refreshes
                sessionStorage.setItem('currentUser', JSON.stringify(user));
                this.currentUserSubject.next(user);
                return user;
            }));
    }

    updateCurrentUser(user) {
        sessionStorage.setItem('currentUser', JSON.stringify(user));
        this.currentUserSubject.next(user);
        return true;
    }

    register(username, password, email, phoneNumber, addressStreet, addressCity) {
        var body = {
            username: username,
            password: password,
            email: email,
            phoneNumber: phoneNumber,
            addressStreet: addressStreet,
            addressCity: addressCity
        };

        return this.http.post<any>(this.baseUrl + "users/", body)
            .pipe(map(user => {
                // store user details and jwt token in local storage to keep user logged in between page refreshes
                return user;
            }));
    }


    logout() {
        sessionStorage.removeItem('currentUser');
        this.currentUserSubject.next(null);
    }
}
