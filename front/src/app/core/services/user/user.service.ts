import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({ providedIn: 'root' })
export class UserService {
    baseUrl = "http://127.0.0.1:8000/api/users/";

    constructor(private http: HttpClient) { }

    getByID(id) {
        return this.http.get(this.baseUrl + id + '/')
    }

    update(user) {
        const body = { 
            "user": {
                username: user.username,
                email: user.email,
                first_name: user.first_name,
                last_name: user.last_name,
                phoneNumber: user.phoneNumber,
                addressStreet: user.addressStreet,
                addressCity: user.addressCity,
            }
        };
        return this.http.patch(this.baseUrl + user.id + '/', body);
    }
}