import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { slideInAnimation } from './route-animation';

import { AuthenticationService } from './core/services/authentication'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [ /*slideInAnimation*/ ]
})
export class AppComponent {

  currentUser: any;

  constructor(
      private router: Router,
      private authenticationService: AuthenticationService
  ) {
      this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
      if (this.authenticationService.currentUserValue == null) { 
        this.router.navigate(['/login']);
    }
  }

  logout() {
      this.authenticationService.logout();
      this.router.navigate(['/login']);
  }

  public getRouterOutletState(outlet) {
    return outlet.isActivated ? outlet.activatedRoute : '';
  }
}
