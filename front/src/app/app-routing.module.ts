import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { HomeComponent } from './modules/home';
import { ProfileComponent } from './modules/profile';
import { LoginComponent } from './modules/login';
import { RegisterComponent } from './modules/register';
import { TaskComponent } from './modules/tasks';
import { OrderComponent } from './modules/order';

const routes: Routes = [
  { path: '', component: HomeComponent  },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent  },
  { path: 'tasks', component: TaskComponent },
  { path: 'order', component: OrderComponent}, 
  { path: 'profile', component: ProfileComponent},
  // otherwise redirect to home
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
