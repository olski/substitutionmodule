import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthenticationService } from '../../core/services/authentication'
import { OrderService } from '../../core/services/order'
import { SelectedTabService } from '../../core/services/selectedTab'

@Component({
	selector: 'app-header',
	templateUrl: './header.component.html'
})
export class HeaderComponent implements OnInit {
	currentUser: any;
	isSecondTab: boolean;
	constructor(
		private router: Router,
		private authenticationService: AuthenticationService,
		private selectedTabService: SelectedTabService,
		private orderService: OrderService

	) {
		this.authenticationService.currentUser.subscribe(x => this.currentUser = x);

		this.selectedTabService.selectedIndex.subscribe(x => {
			if (x == 2)
				this.isSecondTab = true;
			else
				this.isSecondTab = false;
		});
	}
	ngOnInit() {

	}

	logout() {
		this.authenticationService.logout();
		this.router.navigate(['/login']);
	}

	addOrder() {
		this.selectedTabService.changeTab(2);
		this.router.navigate(['/']);
	}

	sleep(milliseconds) {
		return new Promise(resolve => setTimeout(resolve, milliseconds))
	}

	fakeData() {
		this.createFakeUsers();
		this.sleep(750).then(() => {
			this.createFakeOrders();
		})
	}

	createFakeOrders() {
		var orders = [
			{
				creator: "1",
				order_name: "Nieobecność...",
				date_start: "2019-09-25 06:00",
				date_until: "2019-09-25 06:00",
				description: "Lorep Ipsium",
				tasks: new Object(),
				order_type: "Wyjazd służbowy",
			},
			{
				creator: "1",
				order_name: "Starszy Pan",
				date_start: "2019-09-25 06:00",
				date_until: "2019-09-25 06:00",
				description: "Lorep Ipsium",
				tasks: new Object(),
				order_type: "Wyjazd służbowy",
			},
			{
				creator: "1",
				order_name: "Delegacja",
				date_start: "2019-09-15 06:00",
				date_until: "2019-09-27 06:00",
				description: "Lorep Ipsium",
				tasks: new Object(),
				order_type: "Wyjazd służbowy",
			},
			{
				creator: "2",
				order_name: "Wyjazd służbowy",
				date_start: "2019-09-25 06:00",
				date_until: "2019-09-25 06:00",
				description: "Lorep Ipsium3",
				tasks: new Object(),
				order_type: "Wyjazd służbowy",
			},
			{
				creator: "2",
				order_name: "Starszy Pan",
				date_start: "2019-01-02 06:00",
				date_until: "2019-05-05 06:00",
				description: "Lorep Ipsium",
				tasks: new Object(),
				order_type: "Inne",
			},
			{
				creator: "3",
				order_name: "Wakacje bez pieseła",
				date_start: "2019-09-25 06:00",
				date_until: "2019-09-25 06:00",
				description: "Lorep Ipsium3",
				tasks: new Object(),
				order_type: "Wyjazd służbowy",
			}
		];

		var taskses1 = [
			{ title: "Podlać kwiatki", description: "Podlać kwiatki w dużym pokoju, paprotkę dużo!", date: "2019-11-06T23:00:00Z", type_task: "Flora" },
			{ title: "Wyjść z piesełem", description: "Pimpuś musi 15 razy dziennie. :c", date: "2019-11-06T23:00:00Z", type_task: "Fauna" },
			{ title: "Nakarmić rybki", description: "Rekin jest weganem, ręki nie ruszy...", date: "2019-11-06T23:00:00Z", type_task: "Fauna" },
		];

		var taskses2 = [
			{ title: "Zrobić zakupy", description: "Podlać kwiatki w dużym pokoju, paprotkę dużo!", date: "2019-11-06T23:00:00Z", type_task: "Zakupy" },
			{ title: "Porozmawiać z dziadkiem", description: "Dziadzia lubi oglądać telewizję.", date: "2019-11-06T23:00:00Z", type_task: "Flora" },
		];

		var taskses3 = [
			{ title: "Umyć podłogę", description: "Pod umywalką jest płyn do paneli, a pod zlewem do płytek.", date: "2019-11-06T23:00:00Z", type_task: "Sprzatanie" },
			{ title: "Wyjść z psem", description: "3x1 po 15 minut.", date: "2019-11-06T23:00:00Z", type_task: "Fauna" },
			{ title: "Pośpiewać z kanarkiem", description: "Koliberek", date: "2019-11-06T23:00:00Z", type_task: "Fauna" },
		];
		orders.forEach(order => {
			switch (order.creator) {
				case "1":
					order.tasks = taskses1;
					break;
				case "2":
					order.tasks = taskses2;
					break;
				case "3":
					order.tasks = taskses3;
			}

			this.orderService.create(order).pipe()
				.subscribe(
					data => {
					},
					error => {
					});

		});
	}

	createFakeUsers() {
		this.authenticationService.register("Jan",
			"piesek12",
			"test@test.com",
			"123123123",
			"St. Exampul",
			"25/1"
		).pipe()
			.subscribe(
				data => {
				},
				error => {
				});

		this.authenticationService.register("Weronika",
			"piesek12",
			"weronika@test.com",
			"123123123",
			"St. Pszyklad",
			"2312"
		).pipe()
			.subscribe(
				data => {
				},
				error => {
				});

		this.authenticationService.register("Grzech",
			"piesek12",
			"grzech@test.com",
			"123123123",
			"St. Pszykt",
			"1"
		).pipe()
			.subscribe(
				data => {
				},
				error => {
				});
	}

}
