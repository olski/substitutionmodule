import { Component } from '@angular/core';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';

import { AuthenticationService } from '../../core/services/authentication'
import { SelectedTabService } from '../../core/services/selectedTab'
import { OrderService } from '../../core/services/order'
import { UserService } from '../../core/services/user'
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
    templateUrl: 'firstTab.component.html',
    selector: 'first-tab'
})
export class FirstTabComponent {

    visibleOrders: Array<Object> = [];
    orderStore: Array<Object> = [];
    orderTypes = [];
    selectedOrderType: string;
    currentMask: string;
    selectedTab;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private authenticationService: AuthenticationService,
        private orderService: OrderService,
        private selectedTabService: SelectedTabService,
        private userService: UserService,
        private _snackBar: MatSnackBar

    ) {
        if (this.authenticationService.currentUserValue == null) {
            this.router.navigate(['/login']);
        }

        this.getNotReservedOrders();

        this.selectedTabService.selectedIndex.subscribe(x => {
            if (x == 0) {
                this.selectedTab = 0;
                this.getNotReservedOrders();
            }
        });
        this.selectedOrderType = this.orderTypes[0];
        var creator = this.getCreatorName(1);
    }

    getCreatorName(id) {
        return this.userService.getByID(id);
    }

    getNotReservedOrders = () => {
        var userData = this.authenticationService.currentUserValue;
        let userId = userData.user.id;
        this.orderService.getNotReserved(userId).subscribe(
            data => {
                this.orderStore = [];
                for (let item in data) {
                    this.orderStore.push(data[item]['order']);
                    let id = this.orderStore.indexOf(data[item]['order']);
                    this.getCreatorName(this.orderStore[id]['creator']).subscribe(
                        data => {
                            var userName = data['username'];
                            this.orderStore[id]['creator_name'] = userName;
                        },
                        error => {
                            console.log(error);
                        }
                    )
                }
                this.visibleOrders = [...this.orderStore];
            },
            error => {
                console.log(error);
            }
        )
    }

    hideOrder(order) {
        let id = this.visibleOrders.indexOf(order);
        this.visibleOrders.splice(id, 1);
    }

    takeOrder(order) {
        var userData = this.authenticationService.currentUserValue;
        order.performer = userData.user.id;
        order.taken_on = new Date();

        this.orderService.update(order).pipe()
            .subscribe(
                data => {
                    this._snackBar.openFromComponent(SnackBarComponent, {
                        duration: 5000,
                    });
                    let id = this.visibleOrders.indexOf(order);
                    this.visibleOrders.splice(id, 1);
                },
                error => {
                    console.log(error);
                });
    }

    sendUserIdToProfileComponent(id) {
        let navigationExtras: NavigationExtras = {
            queryParams: {
                "id": id
            }
        };
        this.router.navigate(['/profile'], navigationExtras);
    }

    filterOrders(mask) {
        if (!mask || mask == "") {
            this.currentMask = "";
            this.visibleOrders = this.orderStore;
            return;
        }
        this.currentMask = mask;
        var filteredOrders;
        if (this.selectedOrderType == "all") {
            filteredOrders = this.orderStore.filter(order => order['order_name'].toLowerCase().includes(mask.toLowerCase()));
        } else {
            filteredOrders = this.orderStore.filter(order => {
                var filteredTasks = order['tasks'].filter(task => {
                    return task.type_task == this.selectedOrderType;
                });

                if (filteredTasks && filteredTasks.length > 0)
                    return order;
            });
        }
        this.visibleOrders = filteredOrders;
    }

    onTypeChanged(args) {
        this.selectedOrderType = args.currentTarget.value;

        this.filterOrders(this.currentMask);

        if (this.selectedOrderType == "all")
            return;

        var filteredOrders = this.visibleOrders.filter(order => {
            var filteredTasks = order['tasks'].filter(task => {
                return task.type_task == this.selectedOrderType;
            });

            if (filteredTasks && filteredTasks.length > 0)
                return order;
        });
        this.visibleOrders = filteredOrders;
    }
}

@Component({
    selector: 'snackBarComponent',
    templateUrl: 'snackBarComponent.html',
    styles: [`
      .snackBarTheme {
        color: GreenYellow;
      }
    `],
})
export class SnackBarComponent { }