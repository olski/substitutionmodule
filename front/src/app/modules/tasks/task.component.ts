import { Component, OnInit ,Input} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { ModalService } from '../../core/services/modal'


@Component({ templateUrl: 'task.component.html', selector: 'task-selector', })
export class TaskComponent implements OnInit {
    newTaskForm: FormGroup;
    loading = false;
    submitted = false;
    returnUrl: string;
    error: string;
    @Input()
    task;
    modalTask: string;
    currentDate;

    task_types = [ 'Fauna', 'Flora', 'Sprzątanie', 'Zakupy', 'Inne' ];

    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private modalService: ModalService,
    ) {
    }

    ngOnInit() {
        this.newTaskForm = this.formBuilder.group({
            title: [null, Validators.required],
            description: [null, Validators.required],
            type_task: [null, Validators.required],            
            date: [null, Validators.required]         
        });
        
        this.currentDate = new Date();

        
       

        this.returnUrl = this.route.snapshot.queryParams['/home'] || '/';
        this.modalTask = 'task-modal';
    }

    get f() { return this.newTaskForm.controls; }

    onSubmit() {
        
        this.submitted = true;
        if (this.newTaskForm.invalid) {
            return;
        }

        var body = {
            title: this.newTaskForm.controls.title.value,
            description: this.newTaskForm.controls.description.value,
            type_task: this.newTaskForm.controls.type_task.value,
            date: this.newTaskForm.controls.date.value,
        };

        this.newTaskForm.reset();
  
        this.submitted = false;
        this.modalService.close(this.modalTask, body);        
    }
  
}