import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';

import { AuthenticationService } from '../../core/services/authentication'

@Component({ templateUrl: 'register.component.html' })
export class RegisterComponent implements OnInit {
    registerForm: FormGroup;
    loading = false;
    submitted = false;
    returnUrl: string;
    error: string;

    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private authenticationService: AuthenticationService
    ) {
    }

    ngOnInit() {
        this.registerForm = this.formBuilder.group({
            username: ['', Validators.required],
            password: ['', Validators.required],
            confirmPassword: ['', Validators.required],
            email: ['', Validators.compose([Validators.required, Validators.email])],
            phoneNumber: [''],
            addressStreet: ['', Validators.required],
            addressCity: ['', Validators.required],
        },
            {
                validator: this.validateConfirmPassword('password', 'confirmPassword')
            }
        );

        this.returnUrl = '/login';
    }

    validateConfirmPassword(password: string, confirmPassword: string) {
        return (formGroup: FormGroup) => {
            const control = formGroup.controls[password];
            const matchingControl = formGroup.controls[confirmPassword];

            if (matchingControl.errors && !matchingControl.errors.mustMatch) {
                return;
            }

            if (control.value !== matchingControl.value) {
                matchingControl.setErrors({ mustMatch: true });
            } else {
                matchingControl.setErrors(null);
            }
        }
    }

    // convenience getter for easy access to form fields
    get f() { return this.registerForm.controls; }

    onSubmit() {
        this.submitted = true;

        // stop here if form is invalid

        if (this.registerForm.invalid) {
            return;
        }

        this.loading = true;


        this.authenticationService.register(this.f.username.value,
            this.f.password.value,
            this.f.email.value,
            this.f.phoneNumber.value,
            this.f.addressStreet.value,
            this.f.addressCity.value
        )
            .pipe(first())
            .subscribe(
                data => {
                    let navigationExtras: NavigationExtras = {
                        queryParams: {
                            "registered": 'true'
                        }
                    };
                    this.router.navigate([this.returnUrl], navigationExtras);
                },
                error => {
                    for (var key in error.error) {
                        this.error = error.error[key];
                    }
                    this.loading = false;
                });
    }

}
