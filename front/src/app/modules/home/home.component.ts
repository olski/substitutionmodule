import { Component } from '@angular/core';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';

import { AuthenticationService } from '../../core/services/authentication'
import { SelectedTabService } from '../../core/services/selectedTab'

import { MatTabChangeEvent } from '@angular/material';

@Component({ templateUrl: 'home.component.html' })
export class HomeComponent {
    orders: Array<any>;
    selectedTab;
    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private authenticationService: AuthenticationService,
        private selectedTabService: SelectedTabService
    ) {
        if (this.authenticationService.currentUserValue == null) {
            this.router.navigate(['/login']);
        }
        this.selectedTabService.selectedIndex.subscribe(x => {
            this.selectedTab = x;
        }
        );
    }

    tabChanged(tabChangeEvent: MatTabChangeEvent): void {
        this.selectedTabService.changeTab(tabChangeEvent.index);
    }
}