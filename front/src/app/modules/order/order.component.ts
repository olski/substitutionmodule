import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthenticationService } from '../../core/services/authentication';
import { OrderService } from '../../core/services/order'
import { ModalService } from '../../core/services/modal';
import { SelectedTabService } from 'src/app/core/services/selectedTab';


@Component({ templateUrl: 'order.component.html', selector: 'order-selector', })
export class OrderComponent implements OnInit {
    orderForm: FormGroup;
    returnUrl: string;
    error: string;
    tasks = [];
    orderTypes = [];
    selectedOrderType: string;
    modalTask: string = 'task-modal';

    constructor(
        private formBuilder: FormBuilder,
        private router: Router,
        private route: ActivatedRoute,
        private authenticationService: AuthenticationService,
        private orderService: OrderService,
        private modalService: ModalService,        
        private selectedTabService: SelectedTabService
    ) {
        if (!this.authenticationService.currentUserValue) { 
            this.router.navigate(['/login']);
        }
    } 

    ngOnInit() {

        this.returnUrl = this.route.snapshot.queryParams['/'] || '/';
        this.orderForm = this.formBuilder.group({
            orderName: ['', Validators.required],
            dateStart: ['', Validators.required],
            dateUntil: ['', Validators.required],
            comment: ['', Validators.nullValidator],
            order_type: ['', Validators.nullValidator],
        });          
        
        this.orderTypes = [ "Wyjazd służbowy", "L4", "Wakacje", "Inne" ];
    }
      

    get f() { return this.orderForm.controls; }
    
    onSubmit() {
        if (this.orderForm.invalid) {
            return;
        }
        let sessionUser = JSON.parse(sessionStorage.getItem('currentUser'));
        var body = {
            creator: sessionUser.user.id,
            sessionToken: sessionUser.token,
            order_name: this.orderForm.controls.orderName.value,
            date_start: this.orderForm.controls.dateStart.value,
            date_until: this.orderForm.controls.dateUntil.value,
            description: this.orderForm.controls.comment.value,
            tasks: this.tasks,
            order_type: this.orderForm.controls.order_type.value,
        };

        
        this.orderService.create(body).pipe()
        .subscribe(
            data => {
                this.selectedTabService.changeTab(0);
            },
            error => {
                this.error = error;
            });
    }

    onRadioChanged(args) {
        this.selectedOrderType = args.currentTarget.value;
    }

    addTask(id: string){
        this.modalService.open(id, this);        
    }

    cancelBtn(){
        this.router.navigate([this.returnUrl]);
    }
}