import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthenticationService } from '../../core/services/authentication'
import { UserService } from 'src/app/core/services/user';

@Component({ templateUrl: 'profile.component.html' })
export class ProfileComponent implements OnInit {

    userForm: FormGroup;
    editMode: boolean;
    user;
    userId: string;

    constructor(
        private formBuilder: FormBuilder,
        private router: Router,
        private route: ActivatedRoute,
        private authenticationService: AuthenticationService,
        private userService: UserService,
    ) {
        this.user = {
            addressCity: "",
            addressStreet: "",
            email: "",
            first_name: "",
            id: "",
            last_name: "",
            phoneNumber: "",
            username: ""
        };

        this.route.queryParams.subscribe(params => {
            if (params["id"] && params["id"] != authenticationService.currentUserValue.user.id) {
                this.editMode = false;
                this.userId = params["id"];
                this.getUserById();
            }
            else if (!params["id"]) {
                this.user = authenticationService.currentUserValue.user;
                this.editMode = true;
                if(this.userForm) {
                    Object.keys(this.userForm.controls).forEach((controlName) => {
                        this.userForm.controls[controlName].enable();
                    });
                }
            }
        });
        if (!this.userId) {
            this.user = authenticationService.currentUserValue.user;
            this.editMode = true;
        }


    }

    ngOnInit() {
        if(!this.editMode) {
            this.userForm = this.formBuilder.group({
                addressCity: [{value: '', disabled: true}, ''],
                addressStreet: [{value: '', disabled: true}, ''],
                email: [{value: '', disabled: true}, ''],
                first_name: [{value: '', disabled: true}, ''],
                id: [{value: '', disabled: true}, ''],
                last_name: [{value: '', disabled: true}, ''],
                phoneNumber: [{value: '', disabled: true}, ''],
                username: [{value: '', disabled: true}, '']
            });
        }
        else {
            this.userForm = this.formBuilder.group({
                addressCity: ['', ''],
                addressStreet: ['', ''],
                email: ['', ''],
                first_name: ['', ''],
                id: ['', ''],
                last_name: ['', ''],
                phoneNumber: ['', ''],
                username: ['', '']
            });
        }
    }

    get f() { return this.userForm.controls; }

    getUserById() {
        this.userService.getByID(this.userId).subscribe(
            data => {
                this.user = data;
            },
            error => {
                console.log(error);
            }
        )
    }

    update() {
        var newUser = {
            addressCity: this.f.addressCity.value ? this.f.addressCity.value : this.user.addressCity,
            addressStreet: this.f.addressStreet.value ? this.f.addressStreet.value : this.user.addressStreet,
            email: this.f.email.value ? this.f.email.value : this.user.email,
            first_name: this.f.first_name.value ? this.f.first_name.value : this.user.first_name,
            id: this.user.id,
            last_name: this.f.last_name.value ? this.f.last_name.value : this.user.last_name,
            phoneNumber: this.f.phoneNumber.value ? this.f.phoneNumber.value : this.user.phoneNumber,
            username: this.f.username.value ? this.f.username.value : this.user.username
        };

        this.userService.update(newUser).subscribe(
            data => {
                var id = this.user.id;
                this.user = data['user'];
                this.user.id = id;
                var currUser = JSON.parse(sessionStorage.getItem('currentUser'));
                currUser.user = this.user;
                this.authenticationService.updateCurrentUser(currUser);
            },
            error => {
                console.log(error);
            }
        );
    }
}
