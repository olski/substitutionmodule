import { Component } from '@angular/core';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';

import { AuthenticationService } from '../../core/services/authentication'
import { SelectedTabService } from '../../core/services/selectedTab'
import { OrderService } from 'src/app/core/services/order';

import { MatTabChangeEvent } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TaskService } from 'src/app/core/services/task';

@Component({ templateUrl: 'secondTab.component.html', selector: 'second-tab' })
export class SecondTabComponent {
    orderForm: FormGroup;
    taskForm: FormGroup;
    ordersCreated: Array<any> = [];
    ordersToDo: Array<Object> = [];
    selectedTab;
    orderTypes = [];

    constructor(
        private formBuilder: FormBuilder,
        private router: Router,
        private route: ActivatedRoute,
        private authenticationService: AuthenticationService,
        private selectedTabService: SelectedTabService,
        private orderService: OrderService,
        private taskService: TaskService
    ) {
        if (this.authenticationService.currentUserValue == null) {
            this.router.navigate(['/login']);
        }

        this.selectedTabService.selectedIndex.subscribe(x => {
            if (x == 1) {
                this.selectedTab = 1;
                this.getCreatedById();
                this.getReservedById();
            }
        });

        this.taskForm = this.formBuilder.group({
            title: ['', Validators.required],
            type_task: ['', Validators.required],
            description: ['', Validators.nullValidator],
            date: ['', Validators.nullValidator]
        })
        this.orderTypes = ["L4", "Wyjazd służbowy", "Wakacje", "Inne"
        ];

    }

    ngOnInit() {
        this.orderForm = this.formBuilder.group({
            order_name: ['', Validators.required],
            date_from: ['', Validators.required],
            date_until: ['', Validators.required],
            description: ['', Validators.nullValidator],
            performer_id: ['', Validators.nullValidator],
            finished_on: ['', Validators.nullValidator],
            order_type: ['', Validators.nullValidator]
        });

        this.taskForm = this.formBuilder.group({
            title: ['', Validators.required],
            type_task: ['', Validators.required],
            description: ['', Validators.nullValidator],
            date: ['', Validators.nullValidator]
        })

    }

    getCreatedById() {
        this.ordersCreated = [];
        this.orderService.getCreatedById(this.authenticationService.currentUserValue.user.id)
            .subscribe((data) => {
                for (let elem in data) {
                    this.ordersCreated.push(data[elem]['order']);
                }
            });
    }

    getReservedById() {
        this.ordersToDo = [];
        this.orderService.getReservedById(this.authenticationService.currentUserValue.user.id)
            .subscribe((data) => {
                for (let elem in data) {
                    this.ordersToDo.push(data[elem]['order']);
                }
            });
    }

    editOrder(order) {
        order.edit = true;
    }

    saveOrder(order) {
        order.edit = false;
        let sessionUser = JSON.parse(sessionStorage.getItem('currentUser'));

        order.sessionToken = sessionUser.token,
            order.order_name = this.orderForm.controls.order_name.value == '' ? order.order_name : this.orderForm.controls.order_name.value;
        order.date_start = this.orderForm.controls.date_from.value == '' ? order.date_start : this.orderForm.controls.date_from.value.toISOString();
        order.date_until = this.orderForm.controls.date_until.value == '' ? order.date_until : this.orderForm.controls.date_until.value.toISOString();
        order.description = this.orderForm.controls.description.value == '' ? order.description : this.orderForm.controls.description.value;
        order.order_type = this.orderForm.controls.order_type.value == '' ? order.order_type : this.orderForm.controls.order_type.value;

        this.orderService.update(order).subscribe(
            () => {
                console.log('PATH successfull')
            },
            error => {
                console.log(error);
            }
        );
    }

    deleteOrder(order) {
        this.orderService.deleteByID(order.id).subscribe(
            () => {
                let id = this.ordersCreated.indexOf(order);
                this.ordersCreated.splice(id, 1);
            },
            error => {
                console.log(error);
            }
        )
    }

    editTask(task) {
        task.edit = true;
    }

    saveTask(task) {
        task.edit = false;
        let sessionUser = JSON.parse(sessionStorage.getItem('currentUser'));

        task.sessionToken = sessionUser.token,
            task.title = this.taskForm.controls.title.value == '' ? task.title : this.taskForm.controls.title.value;
        task.type_task = this.taskForm.controls.type_task.value == '' ? task.type_task : this.taskForm.controls.type_task.value;
        task.description = this.taskForm.controls.description.value == '' ? task.description : this.taskForm.controls.description.value;
        task.date = this.taskForm.controls.date.value == '' ? task.date : this.taskForm.controls.date.value.toISOString();

        this.taskService.update(task);;
    }

    deleteTask(task, order) {
        this.taskService.deleteByID(task.id).subscribe(
            (val) => {
                var index = order.tasks.indexOf(task);
                order.tasks.splice(index, 1);
            }
        );
    }

    cancelOfReservation(order) {
        order.performer = null;
        this.orderService.update(order).subscribe(
            () => {
                let id = this.ordersCreated.indexOf(order);
                this.ordersToDo.splice(id, 1);
            },
            error => {
                console.log(error);
            }
        )
    }

    doneReservation(order) {
        debugger;
        order.done = true;
        this.orderService.update(order).subscribe(
            () => {
                console.log("Patch successful!");
            },
            error => {
                console.log(error);
            }
        )
    }

    addOrder() {
        this.selectedTabService.changeTab(2);
        this.router.navigate(['/']);
    }

    tabChanged(tabChangeEvent: MatTabChangeEvent): void {
    }
}