from django.contrib.auth import get_user_model
from django.forms.models import model_to_dict
from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from rest_framework import viewsets
from rest_framework.authtoken.models import Token
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.response import Response

from back.api.serializers import UserSerializer, TaskSerializer, OrderSerializer, UserUpdateSerializer
from .models import Task, Order

User = get_user_model()


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer

    def partial_update(self, request, pk=None):
        user = self.get_object()
        userSerializer = UserUpdateSerializer(
            instance=user,
            data=request.data['user']
        )
        userSerializer.is_valid(raise_exception=True)
        userSerializer.save()
        response = userSerializer.data

        return JsonResponse({"user": response})


class CustomObtainAuthToken(ObtainAuthToken):
    def post(self, request, *args, **kwargs):
        response = super(CustomObtainAuthToken, self).post(request, *args, **kwargs)
        token = Token.objects.get(key=response.data['token'])
        user = User.objects.get(id=token.user_id)
        serializer = UserSerializer(user, many=False)
        return Response({'token': token.key, 'user': serializer.data})


class TaskViewSet(viewsets.ModelViewSet):
    queryset = Task.objects.all().order_by('-created_on')
    serializer_class = TaskSerializer

    def partial_update(self, request, pk=None):
        task = self.get_object()
        taskSerializer = TaskSerializer(
            instance=task,
            data=request.data['task']
        )
        taskSerializer.is_valid(raise_exception=True)
        taskSerializer.save()
        response = taskSerializer.data     

        return JsonResponse({"task": response})

class OrderViewSet(viewsets.ModelViewSet):
    queryset = Order.objects.all().order_by('-created_on')
    serializer_class = OrderSerializer

    def list(self, request):  # /orders/
        queryset = Order.objects.all()
        json = []
        for order in queryset:
            tasks = order.order.all()
            data = list(tasks.values())
            order.tasks = data
            json.append({"order": model_to_dict(order)})
        return JsonResponse(json, safe=False)

    def retrieve(self, request, pk=None):  # /orders/id/
        queryset = Order.objects.all()
        order = get_object_or_404(queryset, pk=pk)
        tasks = order.order.all()
        data = list(tasks.values())
        order.tasks = data
        return JsonResponse({"order": model_to_dict(order)})

    def partial_update(self, request, pk=None):
        order = self.get_object()
        orderSerializer = OrderSerializer(
            instance=order,
            data=request.data['order']
        )
        orderSerializer.is_valid(raise_exception=True)
        orderSerializer.save()
        response = orderSerializer.data

        tasks = Task.objects.all()
        addedTasks = []

        for taskData in request.data['order']['tasks']:
            taskInstance = get_object_or_404(tasks, pk=taskData['id'])
            taskSerializer = TaskSerializer(
                instance=taskInstance,
                data=taskData
            )
            taskSerializer.is_valid(raise_exception=True)
            taskSerializer.save()
            addedTasks.append(taskSerializer.data)

        response['tasks'] = addedTasks
        return JsonResponse({"order": response})

    def get_not_reserved(self, userId):
        queryset = Order.objects.exclude(performer_id__isnull=False).exclude(creator_id = userId).exclude(performer_id = userId)
        json = []
        for order in queryset:
            tasks = order.order.all()
            data = list(tasks.values())
            order.tasks = data
            json.append({"order": model_to_dict(order)})
        return JsonResponse(json, safe=False)

    def get_reserved_by(self, performer):
        queryset = Order.objects.all().filter(performer_id=performer)
        json = []
        for order in queryset:
            tasks = order.order.all()
            data = list(tasks.values())
            order.tasks = data
            json.append({"order": model_to_dict(order)})
        return JsonResponse(json, safe=False)

    def get_created_by(self, creator):
        queryset = Order.objects.all().filter(creator_id=creator)
        json = []
        for order in queryset:
            tasks = order.order.all()
            data = list(tasks.values())
            order.tasks = data
            json.append({"order": model_to_dict(order)})
        return JsonResponse(json, safe=False)
