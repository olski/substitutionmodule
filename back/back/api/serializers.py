from django.contrib.auth import get_user_model
from rest_framework import serializers

from .models import Task, Order

User = get_user_model()


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'username', 'email', 'first_name', 'last_name',
                  'password', 'phoneNumber', 'addressStreet', 'addressCity']
        extra_kwargs = {'password': {'write_only': True, 'required': True}}

    def create(self, validated_data):
        user = User.objects.create_user(**validated_data)
        return user


class UserUpdateSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ['username', 'email', 'first_name', 'last_name',
                  'phoneNumber', 'addressStreet', 'addressCity']

    def create(self, validated_data):
        user = User.objects.create_user(**validated_data)
        return user


class TaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = Task
        fields = ['id', 'title', 'type_task', 'description', 'order_id', 'date', 'created_on', 'taken_on',
                  'finished_on', 'performer']

    def create(self, validated_data):
        task = Task.objects.create(**validated_data)
        return task


class OrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order
        fields = ['id', 'description', 'order_name', 'order_type', 'date_start', 'date_until', 'created_on', 'taken_on',
                  'finished_on', 'performer', 'creator', 'done']

    def create(self, validated_data):

        order = Order.objects.create(**validated_data)

        taskArr = self.initial_data['tasks']
        for task in taskArr:
            newTask = TaskSerializer(data=task)
            if newTask.is_valid(raise_exception=True):
                newTask.save(order=order)

        return order
