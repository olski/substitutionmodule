from django.urls import include, path
from rest_framework import routers

from back.api import views
from .views import CustomObtainAuthToken, OrderViewSet, TaskViewSet

router = routers.DefaultRouter()
router.register('users', views.UserViewSet)
router.register('tasks', views.TaskViewSet)
router.register('orders', views.OrderViewSet)

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    path('', include(router.urls)),
    path('auth/', CustomObtainAuthToken.as_view()),
    path('orders/notReserved/<int:userId>/', OrderViewSet.get_not_reserved),
    path('orders/reserved/<int:performer>/', OrderViewSet.get_reserved_by),
    path('orders/creator/<int:creator>/', OrderViewSet.get_created_by),
    path('orders/<int:id>/', OrderViewSet.partial_update),
    path('tasks/<int:id>/', TaskViewSet.partial_update)
]
