from django.contrib.auth.models import AbstractUser
from django.db import models


class User(AbstractUser):
    phoneNumber = models.CharField(max_length=15, blank=True)
    addressStreet = models.CharField(max_length=30, blank=True)
    addressCity = models.CharField(max_length=30, blank=True)
    first_name = models.CharField(max_length=30, blank=True)
    last_name = models.CharField(max_length=30, blank=True)


class Order(models.Model):
    description = models.CharField(max_length=256, null=True, blank=True)
    order_name = models.CharField(max_length=256)
    date_start = models.DateTimeField()
    date_until = models.DateTimeField()
    created_on = models.DateTimeField(auto_now_add=True)
    taken_on = models.DateTimeField(null=True, blank=True)
    finished_on = models.DateTimeField(null=True, blank=True)
    performer = models.ForeignKey(User, null=True, on_delete=models.SET_NULL)
    creator = models.ForeignKey(User, null=False, related_name='userId', on_delete=models.CASCADE)
    tasks = models.TextField(null=True, blank=True)
    order_type = models.CharField(max_length=20)
    done = models.BooleanField(default=False)
    

# Create your models here.
class Task(models.Model):
    order = models.ForeignKey(Order, on_delete=models.CASCADE, related_name='order')
    type_task = models.CharField(max_length=20)
    title = models.CharField(max_length=64, null=True, blank=True)
    description = models.CharField(max_length=256)
    date = models.DateTimeField()
    created_on = models.DateTimeField(auto_now_add=True)
    taken_on = models.DateTimeField(null=True, blank=True)
    finished_on = models.DateTimeField(null=True, blank=True)
    performer = models.ForeignKey(User, null=True, on_delete=models.SET_NULL)
